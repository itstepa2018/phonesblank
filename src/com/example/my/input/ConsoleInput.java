package com.example.my.input;

import java.util.Scanner;

public class ConsoleInput implements Input {
    private Scanner scanner;

    public ConsoleInput() {
        scanner = new Scanner(System.in);
    }

    @Override
    public String inputString(String message) {
        System.out.println(message);
        return scanner.next();
    }

    @Override
    public Integer inputInteger(String message) {
        System.out.println(message);
        String s = scanner.next();
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
