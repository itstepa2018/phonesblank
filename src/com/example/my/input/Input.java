package com.example.my.input;

public interface Input {
    String inputString(String message);

    Integer inputInteger(String message);
}
