package com.example.my.phone;

import com.example.my.connection.Connector;
import com.example.my.main.Main;

import java.util.Objects;

public abstract class Phone implements Callable {
    private double weight;
    private String number;

    protected Phone(double weight) {
        this.weight = weight;
    }

    public abstract void ring();

    @Override
    public Connector incomingCall() {
        ring();
        System.out.println("Incoming call on phone #" + number);
        return new ConnectorImpl();
    }

    @Override
    public Connector outgoingCall(String targetNumber) {
        System.out.println("Phone #" + number + " calling to: " + targetNumber);
        return new ConnectorImpl();
    }

    public double getWeight() {
        return weight;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " (weight:" + getWeight() + "), number: " + getNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return number.equals(phone.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    private class ConnectorImpl implements Connector {
        @Override
        public String sendMessage() {
            return Main.input.inputString(getNumber() + " send message:");
        }

        @Override
        public void receiveMessage(String message) {
            System.out.println(getNumber() + " received message: " + message);
        }
    }
}
