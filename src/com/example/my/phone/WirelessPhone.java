package com.example.my.phone;

public abstract class WirelessPhone extends Phone {
    public static final int MAX_CHARGE = 100;

    private int charge;

    protected WirelessPhone(double weight) {
        super(weight);
        charge = MAX_CHARGE / 2;
    }

    public WirelessPhone(double weight, int charge) {
        super(weight);
        this.charge = Math.max(0, Math.min(charge, MAX_CHARGE));
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }
}
