package com.example.my.phone;

public class NoSuchModelException extends PhoneCreationException {
    public NoSuchModelException(String model) {
        super(model + " phone not found, can not create");
    }
}
