package com.example.my.phone.nokia;

import com.example.my.phone.GsmPhone;
import com.example.my.phone.IPolySound;

public class Nokia3310 extends GsmPhone {
    private static final double weight = 350;

    public Nokia3310() {
        super(weight);
    }

    @Override
    protected IPolySound createRingtone() {
        return new Nokia3310Ringtone();
    }

    @Override
    protected IPolySound createNotification() {
        return new Nokia3310Notification();
    }

    private static class Nokia3310Ringtone implements IPolySound {

        @Override
        public void playSound() {
            System.out.println("Nokia 3310 ringing");
        }
    }

    private static class Nokia3310Notification implements IPolySound {

        @Override
        public void playSound() {
            System.out.println("Nokia 3310 notification sound");
        }
    }
}
