package com.example.my.phone.other;

import com.example.my.phone.WiredPhone;

public class LuxuryWiredPhone extends WiredPhone {
    private Ringtone ringtone;

    protected LuxuryWiredPhone(double weight) {
        super(weight);
        setRingtone(Ringtone.RINGTONE_STANDARD);
    }

    public void setRingtone(Ringtone ringtone) {
        this.ringtone = ringtone;
    }

    @Override
    public void ring() {
        switch (ringtone){
            case RINGTONE_LOW:
                System.out.println("Ring - ringtone low");
                break;
            case RINGTONE_STANDARD:
                System.out.println("Ring - ringtone standard");
            case RINGTONE_HIGH:
                System.out.println("Ring - ringtone high");
        }
    }
}
