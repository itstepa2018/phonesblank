package com.example.my.phone;

public class PhoneCreationException extends Exception {
    public PhoneCreationException(String message) {
        super(message);
    }
}
