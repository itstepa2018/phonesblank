package com.example.my.phone;

import com.example.my.model.Sms;

import java.text.SimpleDateFormat;

public abstract class GsmPhone extends WirelessPhone implements ISms {
    private IPolySound ringtone;
    private IPolySound notification;

    protected GsmPhone(double weight) {
        super(weight);
        this.ringtone = createRingtone();
        this.notification = createNotification();
        if (ringtone == null || notification == null) {
            throw new RuntimeException(getClass().getSimpleName() + " was initialized with null ringtone or notification object");
        }
    }

    protected abstract IPolySound createRingtone();

    protected abstract IPolySound createNotification();

    @Override
    public void ring() {
        ringtone.playSound();
    }

    @Override
    public void sendSms(Sms sms) {
        System.out.println("Phone #" + getNumber() + " sending sms to #" + sms.getNumberTo() + " : " + sms.getText());
    }

    @Override
    public void receiveSms(Sms sms) {
        System.out.println("Phone #" + getNumber() + " at: " + new SimpleDateFormat("dd-MM-YYYY HH:mm:ss")
                .format(sms.getTimeSent()) + " received message : " + sms.getText());
    }

    public IPolySound getRingtone() {
        return ringtone;
    }

    public void setRingtone(IPolySound ringtone) {
        this.ringtone = ringtone;
    }

    public IPolySound getNotification() {
        return notification;
    }

    public void setNotification(IPolySound notification) {
        this.notification = notification;
    }
}
