package com.example.my.phone;

import com.example.my.model.Sms;

public interface ISms {
    void sendSms(Sms sms);

    void receiveSms(Sms sms);
}
