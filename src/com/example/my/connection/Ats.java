package com.example.my.connection;

import com.example.my.phone.Phone;

import java.util.HashMap;
import java.util.Map;

public class Ats {
    private static Ats instance;
    private Map<String, Phone> connectedPhones = new HashMap<>();

    private Ats() {
    }

    public boolean connectPhone(Phone phone) {
        if (connectedPhones.containsKey(phone.getNumber())) {
            return false;
        } else {
            return connectedPhones.put(phone.getNumber(), phone) != null;
        }
    }

    public boolean disconnectPhone(Phone phone) {
        return connectedPhones.remove(phone.getNumber()) != null;
    }

    public boolean isConnected(Phone phone) {
        return connectedPhones.containsKey(phone.getNumber());
    }

    public static Ats getInstance() {
        if (instance == null) {
            instance = new Ats();
        }
        return instance;
    }

    public boolean link(Phone from, Phone to) {
        if ((!connectedPhones.containsKey(from.getNumber()) ||
                (!connectedPhones.containsKey(to.getNumber())))) {
            return false;
        }
        Connector caller = from.outgoingCall(to.getNumber());
        Connector callee = to.incomingCall();
        boolean who = true;
        while (true) {
            String message;
            if (who) {
                message = callee.sendMessage();
                caller.receiveMessage(message);
            } else {
                message = caller.sendMessage();
                callee.receiveMessage(message);
            }
            who = !who;
            if (message.equalsIgnoreCase("000")) {
                System.out.println("Connection finished");
                break;
            }

        }
        return true;
    }
}
